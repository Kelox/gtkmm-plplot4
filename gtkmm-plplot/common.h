#pragma once


namespace Gtk::PLplot::Common
{
  template<typename T>
  class Vector2
  {
    public:
      // CONSTRUCTOR / DECONSTRUCTOR //
      Vector2() : x(0), y(0) {}
      
      Vector2(T px, T py) : x(px), y(py) {}
      
      ~Vector2() = default;
      
      
      // PUBLIC METHODS //
      T getX()
      {
        return x;
      }
      void setX(T px)
      {
        x = px;
      }
      
      
      T getY()
      {
        return y;
      }
      void setY(T py)
      {
        y = py;
      }
      
      
      void set(T px, T py)
      {
        x = px;
        y = py;
      }
    
    private:
      T x, y;
  };
}
